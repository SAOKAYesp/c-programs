#include<stdio.h>
#include<stdlib.h>

//En este programa vamos a calcular la media de 10 números
//que introduzca el usuario.

int main(){

    float suma, total, fin;
  
    printf("En este programa vamos a hacer la media de 10 números.\n");

    for(int i = 1; i <= 10; i++){


        printf("Introduce el número %i: \n", i);
        scanf("%f", &total);
      suma+=total;
    }
    printf("\nLa suma total es: %f", suma);

    fin = suma / 10;

    printf("\nEl resultado de la media es: %f", fin);

        return EXIT_SUCCESS;

    }
