#include<stdio.h>
#include<stdlib.h>

//Este programa intercambiará dos números que introduzcas.


int main(){

    int num1, num2, change1;
  

    printf("Introduce dos números para que se intercambien.\n");
    printf("Introduce el número 1: ");
    scanf("%i", &num1);
    printf("Introduce el número 2: ");
    scanf("%i", &num2);

    change1 = num2;
    num2 = num1;

    printf("\nEl número 1 es %i y el número 2 es %i.\n", change1, num2);

    return EXIT_SUCCESS;
}
