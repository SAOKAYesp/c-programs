#include<stdio.h>
#include<stdlib.h>
#include<stdio_ext.h>

//Este programa guarda y muestra nombres de amigos para una lista con un array a punteros.


int main(){
    char **amigos = NULL;
    char input=0;
    int contador=0;
    char **p;

    do{
        system("clear");
        amigos = (char**) realloc (amigos, (contador+1) * sizeof(char*));
        printf("Introduce el nombre de tus amigos: ");
        input = scanf("%m[a-z A-Z]", amigos+contador++);
        __fpurge(stdin);
    }while(input);

    system("clear");
    p = amigos;

    while(*p != NULL){
        printf("%s \n", *p);
        p++;
    }

    for (char **p=amigos; *p !=NULL; p++)
        free(*p);
        free(amigos);

    return EXIT_SUCCESS;
}
