#include<stdio.h>
#include<stdlib.h>
#define N 6

//Este programa ordena los números del array.

void imprimir(int A[N]){
	
	for (int i=0; i < N; i++){
	printf("%i", A[i]);}
	printf("\n");
	
}


int main(){
	
	int A[N]={5, 2, 7, 1, 3, 4};
	
	imprimir(A);
	
	for(int p=0; p<N-1; p++){ //bucle para hacer pasadas	
		for (int i=0; i<N-1; i++){ //bucle que recorra las partes del array
		if (A[i] > A[i+1]){ //ordenación
			int aux = A[i];
			A[i] = A[i+1];
			A[i+1] = aux;
			}
		}
	}
	
	imprimir(A);
	
	
	
	
	return EXIT_SUCCESS;
}
