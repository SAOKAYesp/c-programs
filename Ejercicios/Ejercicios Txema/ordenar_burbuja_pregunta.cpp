#include<stdio.h>
#include<stdlib.h>
#define N 7

//Este programa pregunta números que guarda en un array para colocarlos.


void ordena(int array[]){

for(int p=0; p<N-1;p++){
		for(int i=0; i<N-1;i++){
			if(array[i]>array[i+1]){
				int aux = array[i];
				array[i]=array[i+1];
				array[i+1]=aux;
			}
		}
		
	}

}

void imprimir(int a[N]){
	
	for(int i=0; i<N; i++){
		printf(" %i ", a[i]);
	}
	printf("\n");
	
}



int main(){
	
	int array[N];
	
	printf("Introduce 7 números.\n");
	
	for( int i=0; i<N; i++){
		printf("Número %i: ",i+1);
		scanf("%i", &array[i]);
		
	}
		
	
	imprimir(array);
	
	ordena(array);
	
	imprimir(array);
	
	
	return EXIT_SUCCESS;
}
