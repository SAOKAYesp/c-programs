#include<stdlib.h>
#include<stdio.h>

//Este programa te dice si el nmero introducido es perfecto o no.



int main (){

    int suma = 0;
    unsigned numero;

    printf("Introduce un número para saber si es perfecto: ");
    scanf(" %u", &numero);


    for (unsigned contador = numero/2; contador > 0; contador--){
        if(numero % contador == 0)
            suma += contador;
    }

    if (numero == suma){
        printf("El número introducido (%u) es perfecto.\n ", numero);
    }else{
        printf("El número introducido (%u) NO es perfecto.\n ", numero);
    }


    return EXIT_SUCCESS;
}
