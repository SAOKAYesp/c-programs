#include<stdio.h>
#include<stdlib.h>

int main (){

    int numero;

    printf("Introduce un número: ");
    scanf("%i", &numero);


    for(int i=numero-1; i>1;i--){
        if (numero%i==0){
            printf("%i dividido entre %i es 0, por lo que NO es primo.\n", numero, i);
            break;
        }else if(i==2 && numero%i==!0){
            printf("El número %i ES PRIMO.\n", numero);
        }else printf("%i dividido entre %i no es 0...\n", numero, i);
    }


    return EXIT_SUCCESS;
}
