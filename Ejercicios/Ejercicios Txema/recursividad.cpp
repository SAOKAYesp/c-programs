#include<stdio.h>
#include<stdlib.h>

//Este programa realiza factoriales mediante una función recursiva.


int bucle(int aux){
	int resultado=0;
	
	if(aux == 1){
		
		return 1;
		
	}else{	
	
	return  aux * bucle(aux-1);	
			
	}
}


int main (){
	
	int numero = 0;
	
	printf("Introduce un número: ");
	scanf("%i", &numero);
	
	
	
	printf("El factorial de %i es: %i\n", numero, bucle(numero));
	
	return EXIT_SUCCESS;
}
