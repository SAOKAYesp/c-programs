#include<stdio.h>
#include<stdlib.h>

int main(){

    int x, result, num;

    printf("Este programa realiza una sucesión.\n");
    printf("Introduce el número de veces que quieres que se realice esta sucesión: ");
    scanf("%i", &num);

    for(int i=0; i<num; i++){
        x = i + 1;
        result = i * x;
        printf (" %i\n",result);
    }



    return EXIT_SUCCESS;
}
