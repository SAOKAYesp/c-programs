#include<stdio.h>
#include<stdlib.h>

//Este programa te dice los 0, 4 y 5 que has introducido.

int main(){

int cantidad, suma, ceros=0, cincos=0, cuatros=0;

printf("Introduce una cantidad de números que quieras: ");
scanf("%i", &cantidad);

for(int i = 0; i<cantidad; i++){
	printf("Número %i: ", i+1);
	scanf("%i", &suma);
	/*
	//ESTA ES OTRA FORMA DE HACERLO 
	if(suma==0){
		ceros++;		
	}else if(suma==4){
		cuatros++;
	}else if(suma==5){
		cincos++;
	}
	*/
	switch(suma){
		case 0:	ceros++;break;
		case 4: cuatros++;break;
		case 5: cincos++;break;
	}
}

printf("Has introducido %i ceros\n", ceros);
printf("Has introducido %i cuatros\n", cuatros);
printf("Has introducido %i cincos\n", cincos);


return EXIT_SUCCESS;
	
}
