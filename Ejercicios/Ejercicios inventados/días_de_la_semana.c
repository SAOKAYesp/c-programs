#include <stdio.h>
#include <stdlib.h>


//Hacer un array y representarlo en pantalla con un puntero.
//Despu�s de eso relacionar cada n�mero del array con un d�a de la semana.

int main(){
	
	int i;
	int numeros[] = { 1, 2, 3, 4, 5, 6, 7};
	//numeros[1] = lunes; //numeros[2] = martes, miercoles = 3, jueves = 4, viernes = 5, sabado = 6, domingo = 7;
	//este es un intento de hacerlo de otra forma.

	
	for (i = 0; i < 7; i++){
		printf(" El d�a %i de la semana es: ", numeros[i]);	
		//se puede poner con la notaci�n de punteros *(numeros+i)
	switch ( i ){
		case 0 : printf ("Lunes\n");
				 break;
		case 1 : printf ("Martes\n");
				 break;
		case 2 : printf ("Mi�rcoles\n");
				 break;
		case 3 : printf ("Jueves\n");
				 break;
		case 4 : printf ("Viernes\n");
				 break;
		case 5 : printf ("S�bado\n");
				 break;
		case 6 : printf ("Domingo\n");
				 break;
	} 

	}
	
	
	
	return EXIT_SUCCESS;
}
