#include<stdio.h>
#include<stdlib.h>

//Este programa lee ficheros
int main(){
	
	char aux;
	FILE *fichero;
	
	
	fichero = fopen("fichero.txt", "r");
	
	if(fichero == NULL){
		printf("Se ha producido un error, fichero no encontrado");
		return EXIT_FAILURE;
	}
	
	while(aux != EOF){
		aux = fgetc(fichero);
		printf("%c", aux);
	}
	
	fclose(fichero);
	
	
	return EXIT_SUCCESS;
}
