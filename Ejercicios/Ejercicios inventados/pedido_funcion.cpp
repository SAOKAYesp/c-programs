#include<stdio.h>
#include<stdlib.h>

//Este programa muestra en pantalla un número pedido en una función.

void pedido ( double **variable){
	double memoria;
	
	printf("Introduce un número: ");
	scanf("%lf", &memoria);
	
	*variable = (double *) malloc (sizeof(double));
	**variable = memoria;
}


int main(){
	
	double *var1 = NULL;
	
	pedido(&var1);
	
	printf("El número es %lf", *var1);
	free(var1);
	
	
	return EXIT_SUCCESS;
}
