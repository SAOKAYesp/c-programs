#include<stdio.h>
#include<stdlib.h>

//Este programa muestra la sucesión de fibonacci hasta el límite que le pongamos.

int fibonacci(int N){
	
	if(N==1){
		return 1;
	}else if(N==0){
		return 0;
	}else{
		return N = fibonacci(N-2)+fibonacci(N-1);
	}	
}



int main(){

	int numero=0;
	int limite;
	 
	printf("Introduce un número límite para la sucesión: ");
	scanf("%i", &limite);
	printf("La secuencia de fibonacci hasta %i es:", limite);
	for(int i=0; i<limite;i++){
	fibonacci(numero+i);
	printf("\n--%i--", fibonacci(numero+i));
}

return EXIT_SUCCESS;
}
