#include <stdlib.h>
#include <stdio.h>

//Este programa está enfocado a usar funciones fuera del int para resolver problemas.

void incrementa (int *interior, int suma){
    *interior += suma;

}



int multiplicar ( int op1, int op2 ){

    return op1 * op2;
}

int operacion ( int op1, int op2, int op3 ){

    return op1 + op3 / op2;
}



int main(){

  int a = 5,
      b = 9,
      c,
      resultado,
      res_final;



c = multiplicar (1 + a, b - 5);

  printf("c = %i\n", c);


incrementa (&a, 7);
printf("a = %i\n", a);


resultado = c / a;

printf("El resultado de c(%i) dividido entre a(%i) es: %i", c, a, resultado);

res_final = operacion ( a - 3, b, c - 2 );
printf("\nEl resultado de la operación es: %i", res_final);



    return EXIT_SUCCESS;
}
