#include<stdio.h>
#include<stdlib.h>
#define N 8

//Este programa ordena por el m�todo de la burbuja.

void burbuja(int a[]){
	
	for(int p=0; p < N-1; p++){
		for(int i=0; i < N-1; i++){
			if(a[i] > a[i+1]){
				int aux = a[i];
				a[i] = a[i+1];
				a[i+1] = aux;
			}
		}	
	}
}



void imprime(int x[]){
	for(int i=0; i<N; i++){
		printf("%i ", x[i]);
	}
	printf("\n");
}



int main(){

	int numeros[N];

	for(int i=0; i<N; i++){
		printf("Introduce el n�mero %i: ", i+1);
		scanf("%i", &numeros[i]);
	}

	imprime(numeros);
	
	burbuja(numeros);
	
	imprime(numeros);



return EXIT_SUCCESS;

}

