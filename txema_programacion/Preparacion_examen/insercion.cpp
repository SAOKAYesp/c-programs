#include<stdio.h>
#include<stdlib.h>

#define N 7


//Este programa ordena por el m�todo de inserci�n.

void insercion(int n[]){
	
	for(int p=0; p<N-1; p++){
		for(int i=p+1; i<N; i++){
			if(n[i]<n[p]){
				int aux= n[i];
				n[i]= n[p];
				n[p]= aux;
			}
		}
	}
	
	
}



void imprime(int x[]){
	for(int i=0; i<N; i++){
		printf("%i ", x[i]);
	}
	printf("\n");
}



int main(){

	int numeros[N];

	for(int i=0; i<N; i++){
		printf("Introduce el n�mero %i: ", i+1);
		scanf("%i", &numeros[i]);
	}

	imprime(numeros);
	
	insercion(numeros);
	
	imprime(numeros);



return EXIT_SUCCESS;

}

