#include<stdio.h>
#include<stdlib.h>

//sumar dos arrays multidimensionales.

int main(){

int array[5][5]={
5,3,7,9,1,
5,3,7,9,1,
5,3,7,9,1,
5,3,7,9,1,
5,3,7,9,1
};

int v2[5][5]={
9,2,5,9,4,
9,2,5,9,4,
9,2,5,9,4,
9,2,5,9,4,
9,2,5,9,4
};

int v3[5][5];


for(int i=0; i<5;i++){
	for(int p=0; p<5; p++){
	v3[i][p]=array[i][p] + v2[i][p];	
	}
}

for(int i=0; i<5;i++){
	printf("\n");
	for(int p=0; p<5; p++){
	printf("%2i ", v3[i][p]);
	}
}

return EXIT_SUCCESS;
}

