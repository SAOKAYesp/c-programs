#include<stdio.h>
#include<stdlib.h>

//Este programa usa punteros a funciones para calcular los 0, 4 y 5.

void funcion(int *ceros, int *cuatros, int *cincos, int suma){
	
	switch(suma){
		case 0: (*ceros)++; break;
		case 4: (*cuatros)++; break;
		case 5: (*cincos)++; break;
	}	
}




int main(){

int suma;
int numero;
int ceros=0;
int cuatros=0;
int cincos=0;

	printf("Introduce la cantidad de n�meros que quieres.");
	scanf("%i", &numero);
	
	for(int i=0; i<numero; i++){
		printf("Introduce el n�mero %i: ", i+1);
		scanf("%i", &suma);
		funcion(&ceros,&cuatros,&cincos,suma);
	}

	printf("Has introducido %i ceros\n", ceros);
	printf("Has introducido %i cuatros\n", cuatros);
	printf("Has introducido %i cincos\n", cincos);
	


return EXIT_SUCCESS;

}

