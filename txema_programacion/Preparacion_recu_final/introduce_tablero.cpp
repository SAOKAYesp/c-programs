#include<stdlib.h>
#include<stdio.h>

#define F 8
#define C 8
#define N 2

void imprimir(char tablero [F][C], int f, int c){
	for (f=0; f<F; f++) {
		for(c=0; c<C; c++) 
			printf(" %c", tablero[f][c]); //tendremos que poner las variables en minúsculas porque son las que se van ampliando progresivamente para que se muestren en el printf.
			printf("\n");		
	}
}


int main(){
	
	int f, c, number, number2;
	char cambio[N];
	
	char tablero [F][C] = { //esto es lo que se va a representar en pantalla.
	
		{ 'O', 'O', 'O', 'O', '1', '1', '1', '1'  },
        { 'O', '1', '1', '1', 'O', 'O', 'O', '1'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'  },
        { 'O', '1', '1', '1', '1', '1', 'O', 'O'  }
    };

	
	imprimir(tablero,f,c);

	printf("\nIntroduce el número para cambiar: ");
	scanf("%c", &cambio[0]);

	printf("\nIntroduce un número: ");
	scanf("%i", &number);
	
	printf("\nIntroduce otro número: ");
	scanf("%i", &number2);
	

	
	printf("\n");
	
	tablero[number][number2] = cambio[0];
	
	imprimir(tablero,f,c);
	
	printf("\n");
	
	return EXIT_SUCCESS;
	
}
