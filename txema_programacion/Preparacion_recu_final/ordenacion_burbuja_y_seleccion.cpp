#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#define N 5

//Ordenación método burbuja

void seleccion(int a[]){

for(int p=0; p<N; p++){
	for(int i=p+1; i<N; i++){
		if(a[p]>a[i]){
			int aux = a[i];
			a[i] = a[p];
			a[p] = aux;
		}
	}
}
}


void burbuja(int a[]){

for(int p=0; p<N-1; p++){
	for(int i=0; i<N-1; i++){
		if(a[i]>a[i+1]){
			int aux = a[i];
			a[i] = a[i+1];
			a[i+1] = aux;
		}
	}
}
}

void imprimir(int a[]){
	
	for(int i=0; i<N;i++){
printf("%i ", a[i]);
}
}

int main(){
int array[N];

for(int i=0; i<N;i++){
printf("Introduce el número %i: ", i+1);
scanf("%i", &array[i]);
}

imprimir(array);

printf("\n");

//burbuja(array);
seleccion(array);

imprimir(array);

return EXIT_SUCCESS;
}
