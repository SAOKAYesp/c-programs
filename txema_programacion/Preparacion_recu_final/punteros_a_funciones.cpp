#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Este programa calcula los espacios y las "a" introducidas. Punteros a funciones.
void funcion(int *a, int *sp, char fr[]){
	
	for(int i=0; i<strlen(fr); i++){
		if(fr[i]=='a'){
			(*a)++;
			printf("%i", *a);
		}else if(fr[i]==' '){
			(*sp)++;
			printf("%i", *sp);
		}
	}
}


int main(){
	
	char frase[50];
	int a=0;
	int space=0;
	
	printf("Introduce una frase: ");
	gets(frase);
	
	funcion(&a, &space, frase);
		
	printf("Has introducido %i aes\n", a);
	printf("Has introducido %i espacios\n", space);
	printf("%s\n", frase);

return EXIT_SUCCESS;
}
