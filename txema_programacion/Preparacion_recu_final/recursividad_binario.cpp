#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Este programa muestra la recursividad. 

void binario(int n){

	if (n>0){
		binario(n/2);
		printf("%i", n%2);	
	}
}


int main(){

int numero;

printf("Introduce un número: ");
scanf("%i", &numero);

binario(numero);

return EXIT_SUCCESS;
}
