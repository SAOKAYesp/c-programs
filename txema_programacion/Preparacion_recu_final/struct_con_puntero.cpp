#include<stdio.h>
#include<stdlib.h>
#include<string.h>

//Este programa muestra el struct, el struct sirve para hacer clases como en BBDD

struct Empleado{
	char DNI[10];
	char *nombre[10];
	char tel[10];
	int salario;
};

typedef int buenas;

int main(){

 buenas extra = 50;
 
 Empleado pepe;

 printf("Introduce tu DNI: ");
 scanf("%s", &pepe.DNI);
 
 printf("Introduce tu nombre: ");
 scanf("%s", pepe.nombre);
 
 printf("Introduce tu Teléfono: ");
 scanf("%s", &pepe.tel);
 
 printf("Introduce el salario que te gustaría tener: ");
 scanf("%i", &pepe.salario);
 

 printf("Tu DNI es %s\n", pepe.DNI);
 printf("Tu nombre es %s\n", pepe.nombre);
 printf("Tu teléfono es %s\n", pepe.tel);
 
 //Aquí este empleado tendrá una paga extra de 50€.
 pepe.salario += extra;
 
 printf("Tu salario deseado es %i\n", pepe.salario);

 
 
return EXIT_SUCCESS;
}
