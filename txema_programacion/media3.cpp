#include <stdio.h>
#include <stdlib.h>

//Este programa calcula una media pero lo hace con una función.

unsigned
sumame(unsigned cantnum, unsigned num, unsigned res){

    for(int i=0; i<cantnum; i++){
        printf("%i-.Numero: ", i+1);
        scanf("%u", &num);
        res += num;
    }
    return res;
}

int main(){

    unsigned cantnum, num , resul, res;

    printf("Cantidad de numeros a poner: ");
    scanf("%u", &cantnum);

    resul = sumame(cantnum, num, res) / cantnum;

    printf("El resultado es:%u.\n", resul);

    return 0;
}
