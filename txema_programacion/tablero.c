#include<stdlib.h>
#include<stdio.h>

#define F 8
#define C 8


int main(){
	
	int f, c;
	
	
	char tablero [F][C] = { //esto es lo que se va a representar en pantalla.
	
		{ 'O', 'O', 'O', 'O', '1', '1', '1', '1'  },
        { 'O', '1', '1', '1', 'O', 'O', 'O', '1'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', '1', 'O', 'O'  },
        { 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'  },
        { 'O', '1', '1', '1', '1', '1', 'O', 'O'  }
    };

	
	
	//si definimos las variables dentro del for, puede dar problemas.
	for (f=0; f<F; f++) {
		for(c=0; c<C; c++) 
			printf(" %c", tablero[f][c]); //tendremos que poner las variables en minúsculas porque son las que se van ampliando progresivamente para que se muestren en el printf.
			printf("\n");		
	}
	
	
	
	
	printf("\n");
	
	return EXIT_SUCCESS;
	
}
